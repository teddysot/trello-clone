import { Button, CssBaseline, InputBase, Paper } from "@mui/material";
import { useContext, useState } from "react";
import StoreApi from "../utils/storeApi";

const InputCard = ({ setIsEditing, listId, type }) => {
  const { addMoreCard, addMoreList } = useContext(StoreApi);
  const [title, setTitle] = useState("");

  const handleOnChange = (event) => {
    setTitle(event.target.value);
  };

  const handleConfirm = () => {
    if (type === "card") {
      addMoreCard(title, listId);
      setIsEditing(false);
      setTitle("");
    } else {
      addMoreList(title);
      setIsEditing(false);
      setTitle("");
    }
  };

  return (
    <div>
      <div>
        <Paper className="pb-8 ml-2 mr-2 mb-2 w-[280px]">
          <InputBase
            onChange={handleOnChange}
            multiline
            fullWidth
            onBlur={() => setIsEditing(false)}
            inputProps={{
              className: "m-2",
            }}
            value={title}
            placeholder={
              type === "card"
                ? "Enter a title of this card..."
                : "Enter a list title..."
            }
          />
        </Paper>
      </div>
      <div className="ml-2 mr-2 mb-2 space-x-5">
        <Button
          className={`text-white font-bold ${
            type === "card"
              ? "bg-[#489c68] hover:bg-[#629c78]"
              : "bg-[#487a9c] hover:bg-[#547f9c]"
          }`}
          onClick={handleConfirm}
        >
          {type === "card" ? "Add" : "Add"}
        </Button>
        <Button
          className="text-white font-bold bg-[#e04d4d] hover:bg-[#e46e6e]"
          onClick={() => setIsEditing(false)}
        >
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default InputCard;
