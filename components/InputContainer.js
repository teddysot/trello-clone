import { Collapse, Paper, Typography } from "@mui/material";
import { useState } from "react";
import InputCard from "./InputCard";

const InputContainer = ({ listId, type }) => {
  const [isEditing, setIsEditing] = useState(false);
  return (
    <div className="mt-2 w-[300px]">
      <Collapse in={isEditing}>
        {type === "card" ? (
          <InputCard setIsEditing={setIsEditing} listId={listId} type={type} />
        ) : (
          <Paper className="w-[300px] bg-[#EBECF0] ml-4 py-2">
            <InputCard
              setIsEditing={setIsEditing}
              listId={listId}
              type={type}
            />
          </Paper>
        )}
      </Collapse>
      <Collapse in={!isEditing}>
        <Paper
          className={`pt-2 pl-2 pr-2 pb-2 ml-2 mr-2 mb-2 cursor-pointer ${
            type === "card"
              ? "bg-[#489c68] hover:bg-[#629c78]"
              : "bg-[#487a9c] hover:bg-[#547f9c]"
          }`}
          elevation={0}
          onClick={() => setIsEditing(!isEditing)}
        >
          <Typography className="font-bold text-white">
            {type === "card" ? "+ Add Card" : "+ Add List"}
          </Typography>
        </Paper>
      </Collapse>
    </div>
  );
};

export default InputContainer;
