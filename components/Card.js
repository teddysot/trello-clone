import React from "react";
import { Paper } from "@mui/material";
import { Draggable } from "react-beautiful-dnd";

const Card = ({ card, index }) => {
  return (
    <Draggable draggableId={card.id} index={index}>
      {(provided) => (
        <div
          ref={provided.innerRef}
          {...provided.dragHandleProps}
          {...provided.draggableProps}
        >
          <Paper className="pt-2 pl-2 pr-2 pb-4 m-2">{card.title}</Paper>
        </div>
      )}
    </Draggable>
  );
};

export default Card;
