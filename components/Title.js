import React, { useContext, useState } from "react";
import { MoreHoriz } from "@mui/icons-material";
import { InputBase, Typography } from "@mui/material";
import StoreApi from "../utils/storeApi";

const Title = ({ title, listId }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [newTitle, setNewTitle] = useState(title);
  const { updateListTitle } = useContext(StoreApi);

  const handleOnChange = (event) => {
    setNewTitle(event.target.value);
  };

  const handleOnBlur = () => {
    updateListTitle(newTitle, listId);
    setIsEditing(false);
  };

  return (
    <div>
      {isEditing ? (
        <div>
          <InputBase
            onChange={handleOnChange}
            autoFocus
            inputProps={{
              className: "m-2 text-xl font-bold focus:bg-[#ddd]",
            }}
            value={newTitle}
            fullWidth
            onBlur={handleOnBlur}
          />
        </div>
      ) : (
        <div className="m-2 flex">
          <Typography
            className="flex-grow text-xl font-bold"
            onClick={() => setIsEditing(!isEditing)}
          >
            {title}
          </Typography>
          <MoreHoriz />
        </div>
      )}
    </div>
  );
};

export default Title;
