import { useState } from "react";
import Head from "next/head";
import List from "../components/List";
import store from "../utils/store";
import StoreApi from "../utils/storeApi";
import { nanoid } from "nanoid";
import InputContainer from "../components/InputContainer";
import { DragDropContext } from "react-beautiful-dnd";
import { Droppable } from "react-beautiful-dnd";

export default function Home() {
  const [data, setData] = useState(store);

  const addMoreCard = (title, listId) => {
    const newCardId = nanoid();
    const newCard = {
      id: newCardId,
      title,
    };

    const list = data.lists[listId];
    list.cards = [...list.cards, newCard];

    const newData = {
      ...data,
      lists: {
        ...data.lists,
        [listId]: list,
      },
    };

    setData(newData);
  };

  const addMoreList = (title) => {
    const newListId = nanoid();
    const newList = {
      id: newListId,
      title,
      cards: [],
    };
    const newData = {
      listIds: [...data.listIds, newListId],
      lists: {
        ...data.lists,
        [newListId]: newList,
      },
    };

    setData(newData);
  };

  const updateListTitle = (title, listId) => {
    const list = data.lists[listId];
    list.title = title;

    const newData = {
      ...data,
      lists: {
        ...data.lists,
        [listId]: list,
      },
    };

    setData(newData);
  };

  const handleDragEnd = (result) => {
    const { destination, source, draggableId, type } = result;

    if (!destination) {
      return;
    }

    if (type === "list") {
      const newListIds = data.listIds;
      newListIds.splice(source.index, 1);
      newListIds.splice(destination.index, 0, draggableId);
      return;
    }

    const sourceList = data.lists[source.droppableId];
    const destinationList = data.lists[destination.droppableId];

    if (source.droppableId === destination.droppableId) {
      const draggingCard = sourceList.cards.splice(source.index, 1)[0];
      destinationList.cards.splice(destination.index, 0, draggingCard);

      const newData = {
        ...data,
        lists: {
          ...data.lists,
          [sourceList.id]: destinationList,
        },
      };

      setData(newData);
    } else {
      const draggingCard = sourceList.cards.splice(source.index, 1)[0];
      destinationList.cards.splice(destination.index, 0, draggingCard);

      const newData = {
        ...data,
        lists: {
          ...data.lists,
          [sourceList.id]: sourceList,
          [destination.id]: destinationList,
        },
      };

      setData(newData);
    }
  };

  return (
    <StoreApi.Provider value={{ addMoreCard, addMoreList, updateListTitle }}>
      <DragDropContext onDragEnd={handleDragEnd}>
        <Droppable droppableId="app" type="list" direction="horizontal">
          {(provided) => (
            <div
              className="flex min-h-screen w-full overflow-y-auto"
              ref={provided.innerRef}
              {...provided.droppableProps}
            >
              <Head>
                <title>Trello Clone</title>
                <link rel="icon" href="/favicon.ico" />
              </Head>
              <main>
                <div className="flex">
                  {data.listIds.map((listId, index) => {
                    const list = data.lists[listId];
                    return <List list={list} key={listId} index={index} />;
                  })}
                  <InputContainer type="list" />
                  {provided.placeholder}
                </div>
              </main>
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </StoreApi.Provider>
  );
}
